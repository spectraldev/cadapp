# Cadapp
Aplicacion para el calculo de cupos de divisas asignado

La idea y objetivo de esta aplicacion es poder proveer y mantaner un calculo del cupo viajero al hacer uso de las divisas asignadas por el gobierno.
Las funciones principales y minimas que tendra esta primera version son:

1. Agregar cupos por año.
2. Registro de viajes
3. Asignacion de cupo por grupo desiganados por el gobierno.
4. Agregar consumos realizados.
5. Historial de consumos.