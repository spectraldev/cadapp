﻿var admobid = {};
if (/(android)/i.test(navigator.userAgent)) {
    admobid = { // for Android
        banner: 'ca-app-pub-6869992474017983/9375997553',
        interstitial: 'ca-app-pub-6869992474017983/1657046752'
    };
} else if (/(ipod|iphone|ipad)/i.test(navigator.userAgent)) {
    admobid = { // for iOS
        banner: 'ca-app-pub-6869992474017983/4806197152',
        interstitial: 'ca-app-pub-6869992474017983/7563979554'
    };
} else {
    admobid = { // for Windows Phone
        banner: 'ca-app-pub-6869992474017983/8878394753',
        interstitial: 'ca-app-pub-6869992474017983/1355127956'
    };
}
// click button to call following functions
function getSelectedAdSize() {
    var i = $("#adSize").selectedIndex;
    var items = $("#adSize").options;
    alert("Obteniendo tamaño " + items[i].value);
    return items[i].value;
}
function getSelectedPosition() {
    var i = document.getElementById("adPosition").selectedIndex;
    var items = document.getElementById("adPosition").options;
    //alert("Obteniendo posicion " + parseInt(items[i].value));
    return parseInt(items[i].value);
}
function createSelectedBanner() {
    alert("Creando banner seleccionado");
    var overlap = $('#overlap').checked;
    if (overlap ? overlap = true : overlap = false);
    var offsetTopBar = $('#offsetTopBar').checked;
    if (offsetTopBar ? offsetTopBar = true : offsetTopBar = false);
    AdMob.createBanner({ adId: admobid.banner, overlap: overlap, offsetTopBar: offsetTopBar, adSize: getSelectedAdSize(), position: getSelectedPosition() });
}
function createBannerOfGivenSize() {
    var w = document.getElementById('w').value;
    var h = document.getElementById('h').value;

    AdMob.createBanner({
        adId: admobid.banner,
        adSize: 'CUSTOM', width: w, height: h,
        position: getSelectedPosition()
    });
}
function showBannerAtSelectedPosition() {
    AdMob.showBanner(getSelectedPosition());
}
function showBannerAtGivenXY() {
    var x = document.getElementById('x').value;
    var y = document.getElementById('y').value;
    AdMob.showBannerAtXY(x, y);
}
function prepareInterstitial() {
    var autoshow = document.getElementById('autoshow').checked;
    AdMob.prepareInterstitial({ adId: admobid.interstitial, autoShow: autoshow });
}